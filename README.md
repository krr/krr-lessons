# KRR didactic tool

The KRR didactic tool is a web application made with the purpose to help in teaching students logic, knowledge representation, and modeling.
The system is based on [FO(.)](https://dtai.cs.kuleuven.be/software/idp/fo) language and uses [IDP3](https://dtai.cs.kuleuven.be/software/idp) system.

This application is developed by KULeuven - KRR group.

## Requirements
In order to use this project you will need:

* Python3.6+
* Pip3

Python modules requirements can be found in `requirements.txt`.

## Dependencies

This web application needs additional software in order to provide full features.
This is [KBSws](https://bitbucket.org/krr/kbsws/src/master/).

This is a web service that can be hosted separately.
The web service endpoint can be specified with the `settings.py` script under the field `KBSWS_ENDPOINT`.

## Development

### Running development server

1. Install all required modules:
`pip3 install -r requirements.txt`

2. You can use dummy database `example_db.sqlite3`. You have to move it to `KRR_didactic_tool` and rename to `db.sqlite3`.
`cp example_db.sqlite3 KRR_didactic_tool/db.sqlite3`

3. Navigate to the `KRR_didactic_tool` (there should be the script manage.py):
`cd KRR_didactic_tool`

4. Run the development server:
`python3 manage.py runserver`

More about the development server can be found [here](https://docs.djangoproject.com/en/3.1/intro/tutorial01/#the-development-server).

### Migrations

In case you have changed (or added new) models, you should create migrations and apply them:

1. Creating migrations:
`python manage.py makemigrations`

2. Applying migrations:
`python manage.py migrate`

---

## Production/Deployment

To deploy the web app we suggest using Gunicorn web server. We will make service of Gunicorn. Also, we show how to create an apache proxy for the service. We assume the MySQL server is used.

### Setup the app

First, we will get the app from the git repo and configure it for deployment.

#### Virtual enviromente

1. Create a root directory for venv:

    ```
    mkdir krrdt
    ```
    
    And position to the new dir: `cd krrdt`
    
2. Create venv:

    ```
    python3 -m venv krrdtenv
    ```
    
    If it is not, install: `sudo apt install python3-venv`

3. Activate venv:

    ```
    source krrdtenv/bin/activate
    ```
    
4. Install Django and Gunicorn:

    ```
    pip3 install django gunicorn
    ```
    
#### App settings

1. Stay in the venv

2. Clone the project application:

    ```
    git clone git@bitbucket.org:krr/krr-lessons.git
    ```

3. Open the `settings.py` file, location is `krr-lessons/KRR_didactic_tool/KRR_didactic_tool/settings.py`.
    
4. Update settings to use the MySQL DB:
    
    ```
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'dbname',
            'USER': 'user',
            'PASSWORD': 'password',
            'HOST': '',
            'PORT': '',
            'OPTIONS': { 'charset': 'utf8' }
        }
    }
    ```

5. Turn off debug mode:
    ```
    DEBUG = False
    ```

6. Specify the known hosts:
    ```
    ALLOWED_HOSTS = ['127.0.0.1']
    ```
    **Note** - We suggest deploying with Gunicorn and using Apache proxy so the known host is `localhost`, this can be different if with different deployment techniques.

#### Requirements

4. Install requirements:
    ```
    pip3 install -r requirements.txt
    ```

5. Install mysqlclient for python:
    ```
    pip3 install mysqlclient
    ```

    In case there is an error "mysql_config: not found" install it with: `sudo apt-get install libmysqlclient-dev`
    
#### Database and migrations

1. Navigate to the rood of the project (in this case):
    ```
    cd krr-lessons/KRR_didactic_tool/
    ```
    **Note** - this will be different if you cloned the project under a different directory!

2. Make sure you have MySQL with the account that has privileges to create the tables! (The user from the settings file.)
    
3. Execute migrations:
    ```
    python3 manage.py migrate
    ```
    
    **Note** - In case of problems, clear all migrations and create new, here is the [tutorial](https://simpleisbetterthancomplex.com/tutorial/2016/07/26/how-to-reset-migrations.html). This is not recommended and normally shouldn't happen!!!

4. Create admin user (with the wizard):
    ```
    python3 manage.py createsuperuser
    ```

#### Static files

1. Uncomment and specify the path in the `custom_settings.py` file. E.g:
    ```
    STATIC_ROOT = "full/path/to/krrdt/krr-lessons/static/"
    ```
    or you can use:
    ```
    STATIC_ROOT = os.path.join(BASE_DIR, 'static')
    ```

2. Collect all the static files:
    ```
    python manage.py collectstatic
    ```

#### Test the Gunicorn

1. Test Gunicorn server with command:
    ```
    gunicorn KRR_didactic_tool.wsgi
    ```

    The result should look like this:
    ```
    [2021-01-11 19:27:15 +0100] [1870] [INFO] Starting gunicorn 20.0.4
    [2021-01-11 19:27:15 +0100] [1870] [INFO] Listening at: http://127.0.0.1:8000 (1870)
    [2021-01-11 19:27:15 +0100] [1870] [INFO] Using worker: sync
    [2021-01-11 19:27:15 +0100] [1873] [INFO] Booting worker with pid: 1873
    ```

2. Stop it with `Ctrl-c`

### Deploy with Gunicorn

Here is the [link](https://docs.gunicorn.org/en/stable/run.html) to the documentation about running Gunicorn.
Make sure to use one of the two next options!

#### Run Gunicorn as a daemon

1. To run the `gunicorn` as a daemon using `--daemon` flag:
    ```
    gunicorn your_project.wsgi --daemon
    ```

2. To run Gunicorn with the `n` workers:
    ```
    gunicorn your_project.wsgi --workers=3
    ```

3. To bind it to a specific port us `-b` flag:
    ```
    gunicorn your_project.wsgi --bind=127.0.0.1:8000
    ```

4. If setproctitle is installed you can adjust the name of the Gunicorn process as they appear in the process system table (which affects tools like `ps` and `top`).
    ```
    gunicorn your_project.wsgi --name=your_project
    ```

5. Example of running this project:
    ```
    gunicorn KRR_didactic_tool.wsgi --bind=127.0.0.1:8000 --workers=3 --name=dkrrdt --daemon
    ```

We describe two ways how to make use of Gunicorn daemon.  

**1) Use production script (recommended)**

1. Copy script to preferred location, e.g. Home of the user:
    ```
    cp Production/service.sh ~/
    ```

2. Edit the script paths
    ```
    nano ~/service.sh
    ```
    And update the `APP_ENV_PATH`, `ENV_NAME`, `*_LOG_FILE` and `BACKUP_SETTINGS`. 
    
3. Add execute rights to the script:
    ```
    chmod +x ~/service.sh
    ```

4. Use the script:
    ```
    ./service.sh
    ```

This option is good since it provides an automated way of updating the app.

**2) Make service for Gunicorn**

1. **Important** - For this step, you will need sudo privileges.

2. Exit venv with the command: 
    ```
    deactivate
    ``` 

3. Create file:
    ```
    sudo nano /etc/systemd/system/app_name.service
    ``` 
    
    Where `app_name` is the name of the application e.g. `krrdt`.
    
4. Add to the file:
    ```
    [Unit]
    Description=gunicorn daemon
    After=network.target

    [Service]
    Restart=on-failure
    WorkingDirectory=/home/ubuntu/krrdt/krr-lessons/KRR_Lessons_Tool
    ExecStart=/home/ubuntu/krrdt/krrdtenv/bin/gunicorn KRR_Lessons_Tool.wsgi
    ExecReload=/bin/kill -HUP $MAINPID
    KillSignal=SIGINT

    [Install]
    WantedBy=multi-user.target
    ```

    `WorkingDirectory` is the directory of the app.
    `ExecStart` in this case is the path to the Gunicorn which is installed in venv
    
5. Enable service:
    ```
    sudo systemctl enable krrdt
    ```
    
6. Start service:
    ```
    service krrdt start
    ```
    
7. Check the status:
    ```
    service krrdt status
    ```

This option is good in case you want to fix the version of the app, hence you don't need updates.

### Apache proxy to Gunicorn

1. **Important** - For this step, you will need sudo privileges.

2. Open the default apache site:
    ```
    sudo nano /etc/apache2/sites-available/000-default.conf
    ```
    
    Note that this site can be named differently!
    Also, it can be the case that there is no default site!
    Find more about apache virtual hosts [here](https://httpd.apache.org/docs/current/vhosts/)
    
3. Add the virtual host to the file (or create a new one and later enable it):

    ```
    <VirtualHost *:80>

        ServerName domainname.com

        ProxyRequests Off
        ProxyPreserveHost Off

        <Proxy *>
            Order deny,allow
            Allow from all
        </Proxy>


        #Let apache serve the static fiels
        ProxyPass /favicon.ico !
        ProxyPass /static/ !
        ProxyPass /media/ !

        Alias /static /path/to/the/krrdt/krr-lessons/KRR_didactic_tool/static
        <Directory /path/to/the/krrdt/krr-lessons/KRR_didactic_tool/static>
            Require all granted
        </Directory>

        Alias /media /path/to/the/krrdt/krr-lessons/KRR_didactic_tool/media
        <Directory /path/to/the/krrdt/krr-lessons/KRR_didactic_tool/media>
            Require all granted
        </Directory>

        #Alias /favicon.ico /path/to/the/krrdt/krr-lessons/static/image/favicon.ico

        ProxyPass / http://127.0.0.1:3434/
        ProxyPassReverse / http://127.0.0.1:3434/

        <Location />
            Order allow,deny
            Allow from all
        </Location>


        <Directory /path/to/the/krrdt/krr-lessons>
            Order deny,allow
            Allow from all
            Options -Indexes
        </Directory>
    </VirtualHost>
    ```
    
    Note that we use port 8000, make sure these are matching with the port of the Gunicorn server.
    Also, paths in the example above have to be updated.
    
    **Note** - We used named-based virtual hosting to proxy requests to Gunicorn. If you wanna create prefixes in the path e.g. `example.com/krrdt` to `localhost:8000`, this procedure will not work! However, it will work for subdomains also.

4. Enable the site:
    ```
    a2ensite krrdt.com.conf
    ```

5. Restart apache service
    ```
    sudo systemctl restart apache2
    ```
    
### Updating the app

1. Use the production script `service.sh`, simply run the command `./service.sh update`.

2. **Note** - If the update contains structural changes of config files, this script will probably not work correctly. In this case, recommended is to perform the actions from the script manually, and at the process of restoring the config file update it accordingly.

3. **Note** - Automated updates with the script are not available if you are using Gunicorn as a system service!






