#!/bin/bash

ROOT=/home/didactictool/

APP_ENV_PATH=/home/didactictool/krrdt/
ENV_NAME=krrdtenv

ACCESS_LOG_FILE=/home/didactictool/log/krrdt/access.log
ERROR_LOG_FILE=/home/didactictool/log/krrdt/error.log
LOG_FILE=/home/didactictool/log/krrdt/log.log
BACKUP_SETTINGS=/home/didactictool/backup/custom_settings.py
PORT=3434

case "$1" in

# Start
start)
	#Navigate to the installation directory
	cd $APP_ENV_PATH

	#Activate venv
	source $ENV_NAME/bin/activate

	#Navigate to the project directory
	cd krr-lessons/KRR_didactic_tool/

	#Run service
	gunicorn KRR_didactic_tool.wsgi --bind=127.0.0.1:$PORT --workers=3 --daemon --access-logfile $ACCESS_LOG_FILE --error-logfile $ERROR_LOG_FILE --log-file $LOG_FILE

	#Leav the venv
	deactivate

	#Print the status
	cd $ROOT
	$0 status
	;;

# Stop
stop)
	#Kill the gunicorn server
	pkill gunicorn

	echo "Stopping service!"
	sleep 5

	#Print the status
	cd $ROOT
	$0 status
	;;

# Restart
restart)
	$0 stop
	sleep 2
	$0 start
	;;

# Update
update)
	#Stop the server
	$0 stop
	sleep 2

	#Navigate to the installation directory
	cd $APP_ENV_PATH

	#Navigate to the project directory
	cd krr-lessons/

	#Copy the settings to backup file
	cp KRR_didactic_tool/KRR_didactic_tool/custom_settings.py $BACKUP_SETTINGS

	#Clear the git
	git checkout .

	#Pull update
	git pull

	#Restore the settings
	cp $BACKUP_SETTINGS KRR_didactic_tool/KRR_didactic_tool/custom_settings.py

	#EXECUTE MIGRATIONS AND UPDATE STATIC FILES

	#Navigate to the installation directory
	cd $APP_ENV_PATH

	#Activate venv
	source $ENV_NAME/bin/activate

	#Navigate to the project directory
	cd krr-lessons/KRR_didactic_tool/

	#Run migrations
	python3 manage.py migrate

	#Collect static files
	python manage.py collectstatic

	#Leav the venv
	deactivate

	sleep 1
	#Start the server
	cd $ROOT
	$0 start
	;;

# Status
status)
	# commands to give the status of your service: running/stopped/how busy/...
	echo "Server status is:"
	if pgrep -x "gunicorn" > /dev/null
	then
		echo "Running!"
	else
		echo "Not running!"
	fi
	;;

*)
	echo "Usage: $0 { start | stop | restart | status }"
	exit 1
	;;

esac
