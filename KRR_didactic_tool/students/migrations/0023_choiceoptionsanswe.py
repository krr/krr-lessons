# Generated by Django 3.0.8 on 2020-12-04 17:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0022_auto_20201203_2322'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChoiceOptionsAnswe',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('option', models.IntegerField()),
                ('choiceanswer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='students.ChoiceAnswer')),
            ],
        ),
    ]
