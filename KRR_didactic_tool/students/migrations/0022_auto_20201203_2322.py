# Generated by Django 3.0.8 on 2020-12-03 22:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0021_auto_20201203_2113'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='assignment',
            options={'ordering': ['-end_date']},
        ),
    ]
