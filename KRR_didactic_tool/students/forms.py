"""
    Copyright (C) 2020  KU Leuven - KRR group
    This file is part of KRR didactic tool.

    KRR didactic tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KRR didactic tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KRR didactic tool.  If not, see <https://www.gnu.org/licenses/>.
"""

from crispy_forms.helper import FormHelper
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Profile, TextQuestion, IDPQuestion
from .submodels.answer_models import TextAnswer, IDPAnswer
from django import template
from django.template.defaultfilters import register


class StudentRegisterForm(UserCreationForm):
    """A class that defines a custom Student Registration Form that can be used in the views."""
    email = forms.EmailField()  # default: required=True

    class Meta:
        model = User  # To save the data in the User model
        fields = ['username', 'email', 'first_name', 'last_name', 'password1', 'password2']


class StudentUpdateForm(forms.ModelForm):
    """A class that defines a form that can be used in the views to allow for students to edit their profiles."""
    email = forms.EmailField()

    def __init__(self, *args, **kwargs):
        super(StudentUpdateForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['readonly'] = True
        #self.fields['email'].widget.attrs['readonly'] = True

    class Meta:
        model = User
        fields = ['username', 'email', 'first_name', 'last_name']  # To update username or email


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['image']  # To update the profile image
