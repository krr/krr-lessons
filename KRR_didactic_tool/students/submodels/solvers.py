"""
    Copyright (C) 2020  KU Leuven - KRR group
    This file is part of KRR didactic tool.

    KRR didactic tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KRR didactic tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KRR didactic tool.  If not, see <https://www.gnu.org/licenses/>.
"""

import time

from django.db import models
from django.template.defaultfilters import register


class Solver(models.Model):
    """Class used in meantime while we can't use the webservice solvers yet"""

    @register.filter
    def run(self, form):
        parameters = {
            "Vocabulary": "vocabulary",  # TODO
            "ExternVocabulary": "",  # TODO: ExternVocabulary for student and for teacher apart !!
            "TheoryA": "\nC1 + C1 = C2. //C2 is the double of C1\n",
            "TheoryB": "\nC1 = MIN[:someIntegers]. //C1 is the smallest of sort someIntegers\nC1 + C1 = C2. //C2 is the double of C1\n",
            "Structure": "\nsomeIntegers = {1..5} //Define the type someIntegers as the range 1..5\n"
        }

        return {"ReturnCode": 200, "Result": '', "Error": form['theory'].as_text()}
