"""
    Copyright (C) 2020  KU Leuven - KRR group
    This file is part of KRR didactic tool.

    KRR didactic tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KRR didactic tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KRR didactic tool.  If not, see <https://www.gnu.org/licenses/>.
"""

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone


class Notification(models.Model):
    """An abstract class for notifications (don't make objects from this class)"""
    text = models.TextField(default='')
    #creation_date = models.DateTimeField(default=timezone.now)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class AssignmentNotification(Notification):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    assignment = models.ForeignKey(to='students.Assignment', on_delete=models.CASCADE)


class CourseNotification(Notification):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    course = models.ForeignKey(to='students.Course', on_delete=models.CASCADE)
    update = models.BooleanField(default=False)
