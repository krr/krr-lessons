"""
    Copyright (C) 2020  KU Leuven - KRR group
    This file is part of KRR didactic tool.

    KRR didactic tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KRR didactic tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KRR didactic tool.  If not, see <https://www.gnu.org/licenses/>.
"""

from django.core.mail import send_mass_mail, send_mail
from django.db import models
from django.db.models import Sum
from django.template.defaultfilters import register
from django.urls import reverse
from django.utils import timezone
from django.conf import settings
from django.contrib.auth.models import User

from .course_models import Course
from .notification_models import AssignmentNotification

class Assignment(models.Model):
    """
    A class for assignments belonging to a course involving a title, description, start/end-date, author and course.
    """
    #Desctiption data
    title = models.CharField(max_length=100)
    description = models.TextField()
    #Start and end date-time
    start_date = models.DateTimeField(default=timezone.now)
    end_date = models.DateTimeField(default=timezone.now)
    #Autrhor and course
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, default='', on_delete=models.CASCADE)
    #Create and updated date time
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    #Order assignments 
    #TODO is this the best order?
    class Meta:
        ordering = ['-end_date']

    def __str__(self):
        return self.title

    @register.filter
    def get_max_score(self):
        """Return the max score of this assignment (= sum of scores of all questions)"""
        sum = 0

        textsum = self.textquestion_set.aggregate(Sum('max_score'))['max_score__sum']
        if (textsum) is not None:
            sum += textsum

        choicesum = self.choicequestion_set.aggregate(Sum('max_score'))['max_score__sum']
        if (choicesum) is not None:
            sum += choicesum

        idpsum = self.idpquestion_set.aggregate(Sum('max_score'))['max_score__sum']
        if (idpsum) is not None:
            sum += idpsum
        return sum

    @register.filter
    def get_score_for(self, student):
        """Get the score for this assignment of the given student. If student did not submit, score == 0."""
        score = 0

        for tq in self.textquestion_set.all():
            if tq.textanswer_set.filter(user=student).exists() and tq.textanswer_set.filter(user=student)[0].score:
                score += tq.textanswer_set.filter(user=student)[0].score
        for cq in self.choicequestion_set.all():
            if cq.choiceanswer_set.filter(user=student).exists() and cq.choiceanswer_set.filter(user=student)[0].score:
                score += cq.choiceanswer_set.filter(user=student)[0].score
        for idpq in self.idpquestion_set.all():
            if idpq.idpanswer_set.filter(user=student).exists() and idpq.idpanswer_set.filter(user=student)[0].score:
                score += idpq.idpanswer_set.filter(user=student)[0].score

        return score

    @register.filter
    def get_all_submissions(self):
        return Submission.objects.filter(assignment=self).all()

    @register.filter
    def was_submitted_by(self, student):
        """Check whether this assignment was submitted by given student"""
        #TODO: use exists
        return len(Submission.objects.filter(student=student, assignment=self)) > 0

    @register.filter
    def was_saved_by(self, student):
        """Check whether this assignment was submitted by given student"""
        for tq in self.textquestion_set.all():
            if(tq.textanswer_set.filter(user=student).exists()):
                return True

        for cq in self.choicequestion_set.all():
            if (cq.choiceanswer_set.filter(user=student).exists()):
                return True

        for idpq in self.idpquestion_set.all():
            if (idpq.idpanswer_set.filter(user=student).exists()):
                return True
        
        return False

    @register.filter
    def was_reviewed(self, student):
        """Check whether this assignment was submitted by given student AND reviewed !"""
        submission = Submission.objects.filter(student=student, assignment=self).last()
        
        if submission:
            return submission.review_date is not None
        else:
            return submission is not None

    @register.filter
    def get_submission_of(self, student):
        """Get the submission object of given student for this assignment"""
        return Submission.objects.filter(assignment=self, student=student).last()

    @register.filter
    def is_open(self):
        """Get the submission object of given student for this assignment"""
        if self.start_date < timezone.now() < self.end_date :
            return True
        else:
            return False

    @register.filter
    def is_finished(self):
        """Check whether this assignment is finished or not"""
        return self.end_date < timezone.now()

    @register.filter
    def is_started(self):
        """Check whether this assignment is started or not"""
        return self.start_date < timezone.now()

    #TODO: not needed?
    def get_absolute_url(self):
        """Return the string of the url (=reverse) of the detailed view of this assignment"""
        return reverse('assignment-detail', kwargs={'pk': self.pk})

    def generate_text_for_email(self, update):
        text = ""
        if update:
            text += f"Updated assignment:\n"
        else:
            text += f"New assignment:\n"

        text += self.title + "\n\n"
        #TODO: Add the url to the courses list
        #text += "Check it out:\n" + reverse('courses') + "\n\n"
        text += "Description:\n"
        text += self.description + "\n\n"
        text += "Starts at:\n" + self.start_date.strftime('%Y-%m-%d %H:%M') + "\n\n"
        text += "Ends at:\n" + self.end_date.strftime('%Y-%m-%d %H:%M') + "\n\n"
        text += "Do not reply to this email!"

        return text

    def notify_students(self, update):
        emailNotification = settings.EMAIL_NOTIFICATIONS

        messages = ()

        message = f"New assignment: {self.title}"
        if update:
            message = f"Updated assignment: {self.title}"

        for student in self.course.students.all():
            AssignmentNotification(text=message, user=student, assignment=self).save()
            messages += ((message, self.generate_text_for_email(update), 'didactic-tool@kuleuven.be', [student.email]),)

        if emailNotification and messages:
            send_mass_mail(
                messages,
                fail_silently=False,
            )

class Submission(models.Model):
    """Class for all assignments submitted by students"""
    student = models.ForeignKey(User, on_delete=models.CASCADE)
    assignment = models.ForeignKey(Assignment, on_delete=models.CASCADE)
    submission_date = models.DateTimeField(default=timezone.now)
    review_date = models.DateTimeField(blank=True, null=True)
    counter = models.IntegerField(default=0)

    def generate_text_for_email(self):
        text = ""
        
        text += f"Assignment reviewed:\n" + self.assignment.title + "\n\n"
        #TODO: Add the url to the courses list
        #text += "Check it out:\n" + reverse('courses') + "\n\n"
        text += "Description:\n" + self.assignment.description + "\n\n"
        text += "Starts at:\n" + self.assignment.start_date.strftime('%Y-%m-%d %H:%M') + "\n\n"
        text += "Ends at:\n" + self.assignment.end_date.strftime('%Y-%m-%d %H:%M') + "\n\n"
        text += "Total score:\n" + str(self.assignment.get_max_score()) + "\n\n"
        text += "Your score:\n" + str(self.assignment.get_score_for(self.student)) + "\n\n"
        text += "Do not reply to this email!"

        return text

    def notify_student(self):
        emailNotification = settings.EMAIL_NOTIFICATIONS

        message = f"Assignment reviewed: {self.assignment.title}"
        AssignmentNotification(text=message, user=self.student, assignment=self.assignment).save()
        
        if emailNotification:
            send_mail(
                    message, 
                    self.generate_text_for_email(), 
                    'didactic-tool@kuleuven.be', 
                    [self.student.email],
                    fail_silently=False,
                )


    #TODO: add unique student and assignment

