"""
    Copyright (C) 2020  KU Leuven - KRR group
    This file is part of KRR didactic tool.

    KRR didactic tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KRR didactic tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KRR didactic tool.  If not, see <https://www.gnu.org/licenses/>.
"""

import ast
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from django.template.defaultfilters import register
from django.conf import settings
import requests as rqs
import json

from students.submodels.question_models import TextQuestion, ChoiceQuestion, IDPQuestion, Option

VOC_BODY = "\n"
THEORY_BODY = "\n"
IDP_BASE_ANSWER = "Vocabulary V {\n" \
                  + VOC_BODY + \
                  "\n}\n\n" \
                  "" \
                  "Theory T: V{\n" \
                  + THEORY_BODY + \
                  "\n}\n\n"


class Answer(models.Model):
    """A model that stores answered questions by students"""
    #User and quesiotn
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    #Score and comment of TA
    score = models.IntegerField(blank=True, null=True)
    comment = models.TextField(blank=True, null=True)
    #Create and updated date time
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
        unique_together = ("user", "question")

class TextAnswer(Answer):
    question = models.ForeignKey(TextQuestion, on_delete=models.CASCADE)
    answer = models.TextField()

    @register.filter
    def estimate_text_score(self):
        a = set(self.question.correct_answer) 
        b = set(self.answer)
        c = a.intersection(b)
        return int((float(len(c)) / (len(a) + len(b) - len(c))) * self.question.max_score)

class ChoiceAnswer(Answer):
    question = models.ForeignKey(ChoiceQuestion, on_delete=models.CASCADE)
    answer = models.TextField(blank=True)

    @register.filter
    def is_option_selected(self,option):
        if str(option.id) in self.answer:
            return True
        else:
            return False

    @register.filter
    def estimate_choice_score(self):
        options = self.question.option_set.all()
        correct_options = 0
        correct = 0
        wrong = 0
        for o in options:
            if(o.is_correct):
                correct_options += 1
            if(str(o.id) in self.answer):
                if(o.is_correct):
                    correct += 1
                else:
                    wrong += 1

        if(correct - wrong <= 0):
            return 0
        else:
            return int(self.question.max_score * ((correct-wrong)/correct_options))
        


class IDPAnswer(Answer):
    question = models.ForeignKey(IDPQuestion, on_delete=models.CASCADE)
    additional_vocabulary = models.TextField(default='', blank=True)
    additional_vocabulary_symbols = models.TextField(default='', blank=True)
    theory = models.TextField(default='', editable=True)

    def estimate_idp_score(self):

        webServiceUrl = settings.KBSWS_ENDPOINT

        data = {
            "Solver":"Idp", 
            "Method":"model_expansion_equivalent_definitions_stats", 
            "Parameters": {
                "Vocabulary":self.question.vocabulary,

                "VocabularyCorrect": self.question.additional_vocabulary,
                "TheoryCorrectFunctional": self.question.additional_vocabulary_symbols,
                "TheoryCorrectStatic" : self.question.theory + "\n" + self.question.background_theory,
                
                "VocabularyStudent": self.additional_vocabulary,
                "TheoryStudentFunctional": self.additional_vocabulary_symbols,
                "TheoryStudentStatic" : self.theory + "\n" + self.question.background_theory,

                "Structure": self.question.structure
            }
        }

        score = 0

        try:
            response = rqs.post(webServiceUrl, json=data)
            response.raise_for_status()  # Raises a HTTPError if the status is 4xx, 5xxx
        except (rqs.exceptions.ConnectionError, rqs.exceptions.Timeout):
            #print "Down"
            score = -404
        except rqs.exceptions.HTTPError:
            #print "4xx, 5xx"
            score = -500
        else:
            # No errors  
            result = json.loads(response.text)
            
            if result['ReturnCode'] == 0 :
                #Get result of idp
                idp_result = result["Result"]

                if(idp_result['ReturnCode'] == 0):
                    (correct, unsound, incomplete) = idp_result["Result"].splitlines()

                    c2 = int(correct.split(":")[1]) * 2
                    u = int(unsound.split(":")[1])
                    i = int(incomplete.split(":")[1])

                    score = int((100 - ((100 / c2) * (u + i))) * self.question.max_score / 100)
            else:
                score = -1

        return score