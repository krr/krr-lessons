"""
    Copyright (C) 2020  KU Leuven - KRR group
    This file is part of KRR didactic tool.

    KRR didactic tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KRR didactic tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KRR didactic tool.  If not, see <https://www.gnu.org/licenses/>.
"""

from abc import abstractmethod

from django.db import models
from django.template.defaultfilters import register

from students.submodels.assignment_models import Assignment
from students.submodels.solvers import Solver


class Question(models.Model):
    """
    An abstract class for questions that always belongs to an assignment.
        When extending this method, make sure to declare the 'assignment' property at each subclass level.
        It is important to not declare the assignment attribute in this superclass. Because this would prevent us
        from using for example 'textquestion_set' on an Assignment object.
    """
    title = models.CharField(max_length=100)
    description = models.TextField()
    max_score = models.IntegerField()
    order = models.IntegerField(default=0)
    #Create and updated date time
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @abstractmethod
    def is_answer_correct(self):
        pass

    def copy(self, question):
        """Copy all the attributes of this question into the given question."""
        question.title = self.title
        question.description = self.description
        question.max_score = self.max_score

    def __str__(self):
        return f"{self.title} ({self.pk})"

    @register.filter(name='get_class')
    def get_class(value):
        return value.__class__.__name__


class TextQuestion(Question):
    """A class for text questions related to an assignment object."""
    assignment = models.ForeignKey(Assignment, on_delete=models.CASCADE)
    correct_answer = models.TextField(default='')

    def is_answer_correct(self):
        # TODO
        return False

    @register.filter
    def get_text_answer_for_user(self,user):
        if self.textanswer_set.filter(user=user).exists():
            return self.textanswer_set.filter(user=user)[0]
        else:
            return None

class ChoiceQuestion(Question):
    """A class for questions with multiple options from which students can choose as answer."""
    assignment = models.ForeignKey(Assignment, on_delete=models.CASCADE)

    # TODO: Is this needed?
    def is_answer_correct(self):
        return True
        # TODO: return self.student_answer is not None and self.student_answer.is_correct()

    @register.filter
    def get_choice_answer_for_user(self,user):
        if self.choiceanswer_set.filter(user=user).exists():
            return self.choiceanswer_set.filter(user=user)[0]
        else:
            return None

class Option(models.Model):
    choicequestion = models.ForeignKey(ChoiceQuestion, on_delete=models.CASCADE)  # TODO FK for CQuestion or CAnswer?
    text = models.CharField(blank=False, max_length=100)  # TODO: Is this big enough
    is_correct = models.BooleanField(default=False)


class IDPQuestion(Question):
    # Fields for instructors
    vocabulary = models.TextField(default='', blank=True)
    additional_vocabulary_enabled = models.BooleanField(default=True, blank=True)
    additional_vocabulary = models.TextField(default='', blank=True)
    additional_vocabulary_symbols = models.TextField(default='', blank=True)  # TODO: change to symbol_definitions
    background_theory = models.TextField(default='', blank=True)
    theory = models.TextField(default='', blank=True)
    structure = models.TextField(default='', blank=True)
    solver = models.ForeignKey(Solver, default=1, on_delete=models.CASCADE)  # TODO
    assignment = models.ForeignKey(Assignment, on_delete=models.CASCADE)

    @register.filter
    def get_idp_answer_for_user(self,user):
        if self.idpanswer_set.filter(user=user).exists():
            return self.idpanswer_set.filter(user=user)[0]
        else:
            return None

    def is_answer_correct(self):
        # TODO
        return False
