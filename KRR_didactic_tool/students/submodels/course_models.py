"""
    Copyright (C) 2020  KU Leuven - KRR group
    This file is part of KRR didactic tool.

    KRR didactic tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KRR didactic tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KRR didactic tool.  If not, see <https://www.gnu.org/licenses/>.
"""

from django.core.mail import send_mass_mail 
import django.utils.timezone as timezone
from django.template.defaultfilters import register
from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from django.conf import settings
from .notification_models import CourseNotification

class Course(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    image = models.ImageField(default='default-course.jpg', upload_to='profile_pics')
    students = models.ManyToManyField(User, blank=True)  # Registered students
    start_date = models.DateTimeField(default=timezone.now)
    #Create and updated date time
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    #TODO: not needed?
    def get_absolute_url(self):
        """Return the string of the url (=reverse) of the detailed view of this group"""
        return reverse('course-detail', kwargs={'pk': self.pk})

    @register.filter
    def get_nb_of_assignments_left_for(self, student):
        """Get the number of unsubmitted assignments of this course for the given student."""
        #TODO: Write single query
        return sum(1 for assignment in self.assignment_set.filter(end_date__gte = timezone.now()) if not assignment.was_submitted_by(student))

    def generate_text_for_email(self, update):
        text = ""
        if update:
            text += f"Updated course:\n"
        else:
            text += f"New course:\n"

        text += self.name + "\n\n"
        #TODO: Add the url to the courses list
        #text += "Check it out:\n" + reverse('courses') + "\n\n"
        text += "Description:\n"
        text += self.description + "\n\n"
        text += "Starts at:\n" + self.start_date.strftime('%Y-%m-%d %H:%M') + "\n\n"
        text += "Do not reply to this email!"

        return text

    def notify_students(self, update):
        emailNotification = settings.EMAIL_NOTIFICATIONS

        messages = ()

        #TODO: parametrize the email message 
        if update:
            for user in self.students.all():
                CourseNotification(text=f"Updated course: {self.name}", user=user, course=self, update=update).save()
                messages += ((f"Updated course - {self.name}", self.generate_text_for_email(update), 'didactic-tool@kuleuven.be', [user.email]),)
        else:
            for user in User.objects.all():
                CourseNotification(text=f"New course: {self.name}", user=user, course=self, update=update).save()
                messages += ((f"New course - {self.name}", self.generate_text_for_email(update), 'didactic-tool@kuleuven.be', [user.email]),)

        if emailNotification and messages:
            send_mass_mail(
                messages,
                fail_silently=False,
            )
            
