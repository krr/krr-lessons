"""
    Copyright (C) 2020  KU Leuven - KRR group
    This file is part of KRR didactic tool.

    KRR didactic tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KRR didactic tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KRR didactic tool.  If not, see <https://www.gnu.org/licenses/>.
"""

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib import messages

from students.forms import StudentRegisterForm, StudentUpdateForm, ProfileUpdateForm


def register(request):
    """Render a registration form or register the new student whose information is passed with the given request."""
    if request.method == 'POST':
        form = StudentRegisterForm(request.POST)
        if form.is_valid():
            form.save()  # Saving the user
            username = form.cleaned_data.get('username')
            messages.success(request, f'Your account has been created!')  # info, success, warning, error, debug
            return redirect('login')
    else:
        form = StudentRegisterForm()
    return render(request, 'students/register.html', {'form': form})


@login_required
def profile(request):
    """Render the authenticated student's profile by showing 2 forms: student_form and profile_form"""
    if request.method == 'POST':
        student_form = StudentUpdateForm(request.POST, instance=request.user)
        profile_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)
        if student_form.is_valid() and profile_form.is_valid():
            student_form.save()
            profile_form.save()
            messages.success(request, f'Your account has been updated!')
            return redirect('profile')
    else:
        student_form = StudentUpdateForm(instance=request.user)          # 'instance=' for updating existing objects,
        profile_form = ProfileUpdateForm(instance=request.user.profile)  # instead of making new

    context = {
        'student_form': student_form,
        'profile_form': profile_form
    }

    return render(request, 'students/profile.html', context)
