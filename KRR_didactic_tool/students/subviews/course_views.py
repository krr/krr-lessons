"""
    Copyright (C) 2020  KU Leuven - KRR group
    This file is part of KRR didactic tool.

    KRR didactic tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KRR didactic tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KRR didactic tool.  If not, see <https://www.gnu.org/licenses/>.
"""

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.shortcuts import render
from django.utils import timezone
from django.views.generic import ListView, DetailView, UpdateView
from students.submodels.course_models import Course


class CourseListView(LoginRequiredMixin, ListView):
    # TODO: make a list view per group
    model = Course
    context_object_name = 'courses'
    extra_context = {'now': timezone.now()}


class CourseRegisterView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Course
    template_name = 'students/course_register.html'
    success_url = '/courses'  # TODO message of success
    fields = []

    def form_valid(self, form):
        self.get_object().students.add(self.request.user)  # Register user to the course
        return super().form_valid(form)

    def test_func(self):
        return True
        pass  # TODO: Start date of course already passed


class CourseUnregisterView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Course
    template_name = 'students/course_unregister.html'
    success_url = '/courses'  # TODO message of success
    fields = []

    def form_valid(self, form):
        print(self.request.user.id)
        self.get_object().students.remove(self.request.user)
        return super().form_valid(form)

    def test_func(self):
        return True  # TODO


class CourseDetailView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = Course
    context_object_name = 'course'

    def test_func(self):
        course = self.get_object()
        return course in self.request.user.course_set.all()
