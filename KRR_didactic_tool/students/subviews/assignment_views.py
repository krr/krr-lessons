"""
    Copyright (C) 2020  KU Leuven - KRR group
    This file is part of KRR didactic tool.

    KRR didactic tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KRR didactic tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KRR didactic tool.  If not, see <https://www.gnu.org/licenses/>.
"""

import json

from django import forms
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib import messages
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils import timezone
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.conf import settings
from ast import literal_eval
import requests as rqs
from itertools import chain
from operator import attrgetter

#from students.forms import TextAnswerForm, IDPAnswerForm
from students.submodels.assignment_models import Assignment, Submission
from students.submodels.course_models import Course
from students.submodels.notification_models import AssignmentNotification
from students.submodels.question_models import Question, TextQuestion, ChoiceQuestion, Option, IDPQuestion
from students.submodels.answer_models import Answer, TextAnswer, ChoiceAnswer, IDPAnswer


class AssignmentListView(LoginRequiredMixin, ListView):
    model = Assignment
    # You can specify a template_name = 'students/assignment_list.html'
    # Otherwise, default:  '<app>/<model>_<viewtype>.html' --> 'students/assignment_list.html'
    ordering = ['start_date']  # Add a minus sign if you want to order from newest to oldest
    # TODO: apply some filtering (so that you can differentiate between submitted assignments and unsubmitted

    def get(self, request, *args, **kwargs):
        context = {'courses': self.request.user.course_set.all()}
        return render(request, 'students/assignment_list.html', context)


class SubmissionsListView(LoginRequiredMixin, ListView):
    model = Assignment
    ordering = ['start_date']

    def get(self, request, *args, **kwargs):
        context = {'courses': Course.objects.all()}
        return render(request, 'students/submissions_list.html', context)


class AssignmentDetailView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = Assignment
    context_object_name = 'assignment'
    # TODO: user is this user if this user is not staff, otherwise add parameter 'student' to this func?

    def __init__(self, *args, **kwargs):
        super(AssignmentDetailView, self).__init__(*args, **kwargs)

    def test_func(self):
        """Check whether the connected user is registered to the course to which this assignment belongs."""
        return self.get_object().course in self.request.user.course_set.all()

    def get(self, request, *args, **kwargs):
        """Handle the get request by first setting the necessary objects for context (was not possible in init)"""

        #Get the assignemnt from arguments
        assignment = Assignment.objects.get(pk=self.kwargs['pk'])

        #Sorting questions by the order        
        questions = sorted(
            chain(assignment.textquestion_set.all(), assignment.choicequestion_set.all(), assignment.idpquestion_set.all()),
            key=attrgetter('order'), reverse=False)

        #Prepare context / axxignemnt / questins / time now
        context = {'assignment': assignment,
                   'questions': questions,
                   'now': timezone.now()}

        return render(request, 'students/assignment_detail.html', context)


class AssignmentSubmissionsListView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = Assignment
    template_name = 'students/assignment_submissions_list.html'

    def test_func(self):
        return self.request.user.is_staff

    def get(self, request, *args, **kwargs):
        
        assignment = Assignment.objects.get(pk=self.kwargs['pk'])
        sumbissions = Submission.objects.filter(assignment=assignment).all()

        context = {'assignment': assignment, 
                   'submissions': sumbissions, 
                   'now': timezone.now()}

        return render(request, self.template_name, context)


class AssignmentSubmissionView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = Assignment
    template_name = 'students/assignment_submission_detail.html'

    def test_func(self):
        return self.request.user.is_staff  # TODO: + test that student has submitted for this assignment

    def get(self, request, *args, **kwargs):
        assignment = Assignment.objects.get(pk=self.kwargs['assignment_id'])
        submission = Submission.objects.get(pk=self.kwargs['submission_id'])
        student = User.objects.get(pk=submission.student.id)

        #Sorting questions by the order        
        questions = sorted(
            chain(assignment.textquestion_set.all(), assignment.choicequestion_set.all(), assignment.idpquestion_set.all()),
            key=attrgetter('order'), reverse=False)

        context = {
            'assignment': assignment,
            'submission': submission,
            'student': student,
            'questions': questions,
            'now': timezone.now()
        }

        return render(request, self.template_name, context)


@login_required
#Check the correctness of idp question
def check_idp(request, **kwargs):

    webServiceUrl = settings.KBSWS_ENDPOINT
    numberOfModels = settings.KBSWS_NUMBER_OF_MODELS
    errorKeyWords = settings.ERROR_KEY_WORDS

    ajax_post = literal_eval(request.body.decode("utf-8"))
    question = IDPQuestion.objects.get(pk=ajax_post["QuestionID"])

    data = {
        "Solver":"Idp", 
        "Method":"model_expansion_equivalent_definitions", 
        "Parameters": {
            "NumberOfModels" : numberOfModels,
            "Vocabulary":question.vocabulary,

            "VocabularyCorrect": question.additional_vocabulary,
            "TheoryCorrectFunctional": question.additional_vocabulary_symbols,
            "TheoryCorrectStatic" : question.theory + "\n" + question.background_theory,
            
            "VocabularyStudent":ajax_post["VocabularyStudent"],
            "TheoryStudentFunctional": ajax_post["TheoryStudentFunctional"],
            "TheoryStudentStatic" : ajax_post["TheoryStudentStatic"] + "\n" + question.background_theory,

            "Structure": question.structure
        }
    }

    returnData = {
        'unsound': "",
        'incomplete': "",
        'error': "",
        'warning': ""
    }

    try:
        response = rqs.post(webServiceUrl, json=data)
        response.raise_for_status()  # Raises a HTTPError if the status is 4xx, 5xxx
    except (rqs.exceptions.ConnectionError, rqs.exceptions.Timeout):
        #print "Down"
        returnData['error'] = "ERROR: Web service for IDP check is temporarily down!"
    except rqs.exceptions.HTTPError:
        #print "4xx, 5xx"
        returnData['error'] = "ERROR: Call to IDP check Web service failed!"
    else:
        # No errors  
        result = json.loads(response.text)
        
        if result['ReturnCode'] == 0 :
            #Get result of idp
            idp_result = result["Result"]

            # 1) unsound/incomplete
            if('ReturnCode' in idp_result and idp_result['ReturnCode'] == 0):
                (unsound, incomplete) = str.split(idp_result["Result"], "+++===+++")

                returnData['unsound'] = unsound.strip()
                returnData['incomplete'] = incomplete.strip()

            # 2) Error/Warning
            error_warning_lines = idp_result["Error"].splitlines()
            # Seprate warnings from errors

            error_exists = False

            for ew in error_warning_lines:
                if ew.startswith('Error:'):
                    #print(ew)
                    error_exists = True
                    if any(errorKey in ew for errorKey in errorKeyWords):
                        end_substring = -1
                        if ew.find("At /tmp/") > 0:
                            end_substring = ew.index("At /tmp/")

                        error_text = ew[0 : end_substring] + "\n"
                        if(not error_text in returnData['error']):
                            returnData['error'] += error_text

            if error_exists and len(returnData['error']) == 0:
                returnData['error'] += "There is some error in your formalization that is not captured by the solver! Try again or contact TA!"
                
                
                #Disclude warnings at the moment
                #elif ew.startswith('Warning:'):
                #    returnData['warning'] += ew + "\n"
        else:
            #Return error code of web service
            returnData['error'] = "ERROR: Web service status code (" + str(result['ReturnCode']) + ") - You can check codes out WebService api page (" + webServiceUrl + ")!"
            ##TODO replace the endpoint with the server name

    return HttpResponse(json.dumps(returnData), content_type="application/json")


#Save the answers of the assignment
@login_required
def save_answers(request, **kwargs):

    
    data = literal_eval(request.body.decode("utf-8"))
    assignment = Assignment.objects.get(pk=data['assignmentID'])

    responseData = {"message":"", "status": 0}

    #Check date and time
    if not assignment.is_open():
        responseData["message"] = "You are trying to submit answer out of allowed date and time!"
        responseData["status"] = -1
    else:
        #Try to sace data
        try:
            #Save text answers
            for a in data["textAnswers"]:
                #Check if answer exist 
                question = TextQuestion.objects.get(pk=a["QuestionID"])
                answer, created = TextAnswer.objects.update_or_create(question=question, user=request.user)
                answer.answer = a["TextAnswer"]
                answer.save()

            #Save choice answers
            for a in data["choiceAnswers"]:
                #Check if answer exist 
                question = ChoiceQuestion.objects.get(pk=a["QuestionID"])
                answer, created = ChoiceAnswer.objects.update_or_create(question=question, user=request.user)
                answer.answer = a["ChoiceAnswer"]
                answer.save()

            #Save IDP answers
            for a in data['idpAnswers']:
                question = IDPQuestion.objects.get(pk=a["QuestionID"])
                answer, created = IDPAnswer.objects.update_or_create(question=question, user=request.user)
                answer.additional_vocabulary = a["VocabularyStudent"]
                answer.additional_vocabulary_symbols = a["TheoryStudentFunctional"]
                answer.theory = a["TheoryStudentStatic"]
                answer.save()

            if( data['submit'] == 'true'):
                # Make submission
                submission, created = Submission.objects.update_or_create(assignment=assignment, student=request.user)
                submission.submission_date = timezone.now()
                submission.counter += 1
                submission.save()

                #If everithing went well
                responseData["message"] = "Your data is saved, and submission is created. Fill free to submit again, just keep in mind that we count the number of attempts!"
            else:
                #Just save
                responseData["message"] = "Your data is saved, but submission is NOT created. Don't forget to submit it later!"

        #If some error ocurred while saving data
        except:
            responseData["message"] = "Something went wrong, try again later!"
            responseData["status"] = -1


    return HttpResponse(json.dumps(responseData), content_type="application/json")


#Save the review of assignment
@login_required
def save_submission_review(request, **kwargs):
    
    review = literal_eval(request.body.decode("utf-8"))
    submission = Submission.objects.get(id=review['submissionID'])

    responseData = {"message":"", "status": 0}

    #Check date and time
    if submission.assignment.is_open():
        responseData["message"] = "You are trying to submit review of submission but assignment is still open!"
        responseData["status"] = -1
    else:
    #Try to save data
        try:
            #Save text answers
            for a in review["textReview"]:
                #Check if answer exist 
                answer, created = TextAnswer.objects.update_or_create(pk=a["AnswerID"])
                answer.score = a["Score"]
                answer.comment = a["Comment"]
                answer.save()

            #Save choice answers
            for a in review["choiceReview"]:
                #Check if answer exist 
                answer, created = ChoiceAnswer.objects.update_or_create(pk=a["AnswerID"])
                answer.score = a["Score"]
                answer.comment = a["Comment"]
                answer.save()

            #Save IDP answers
            for a in review["idpReview"]:
                #Check if answer exist 
                answer, created = IDPAnswer.objects.update_or_create(pk=a["AnswerID"])
                answer.score = a["Score"]
                answer.comment = a["Comment"]
                answer.save()

            #Update submission
            submission.review_date = timezone.now()
            submission.save()

            #Notify the students 
            submission.notify_student()

            #If everithing went well
            responseData["message"] = "Your review is saved. Fill free to submit again!"
        #If some error ocurred while saving data
        except Exception as e: 
            responseData["message"] = "Something went wrong, try again later! (" + str(e) + ")"
            responseData["status"] = -1


    return HttpResponse(json.dumps(responseData), content_type="application/json")


#Automated checking of the assignment
@login_required
def automated_checking(request, **kwargs):

    data = literal_eval(request.body.decode("utf-8"))

    submission = Submission.objects.get(id=data['submissionID'])

    responseData = {"message":"", "status": 0, "submission_id": submission.id, "score": 0}

    #Check date and time
    if submission.assignment.is_open():
        responseData["message"] = "You are trying to submit review of submission but assignment is still open!"
        responseData["status"] = -1
    else:
        #Try to sace data
        try:
            #TODO: Try to write singe query sets

            #Score text answers
            for tq in submission.assignment.textquestion_set.all():
                for ta in tq.textanswer_set.filter(user = submission.student):
                    ta.score = ta.estimate_text_score()
                    ta.comment = "This answer is checked by machine, so ther is no comment!"
                    ta.save()

            #Score text answers
            for cq in submission.assignment.choicequestion_set.all():
                for ca in cq.choiceanswer_set.filter(user = submission.student):
                    ca.score = ca.estimate_choice_score()
                    ca.comment = "This answer is checked by machine, so ther is no comment!"
                    ca.save()

            #Score idp answers
            for iq in submission.assignment.idpquestion_set.all():
                for ia in iq.idpanswer_set.filter(user = submission.student):
                    ia.score = ia.estimate_idp_score()
                    ia.comment = "This answer is checked by machine, so ther is no comment!"
                    ia.save()

            #Update submission
            submission.review_date = timezone.now()
            submission.save()

            #Notify the students 
            submission.notify_student()

            #If everithing went well
            responseData["message"] = "Submission: (<b>" + str(submission.id) + "</b>) for user (<b>" + str(submission.student) + "</b>) is evaluated. The score is: [<b>" + str(submission.assignment.get_score_for(submission.student)) + "</b>]."
            responseData["score"] = submission.assignment.get_score_for(submission.student)
        #If some error ocurred while saving data
        except Exception as e: 
            responseData["message"] = "Submission: (<b>" + str(submission.id) + "</b>) for user (<b>" + str(submission.student) + "</b>) evaluation went wrong! Error: (" + str(e) + ")"
            responseData["status"] = -1

    return HttpResponse(json.dumps(responseData), content_type="application/json")


#IDP Answer estimation
@login_required
def idp_estimation(request, **kwargs):

    data = literal_eval(request.body.decode("utf-8"))

    answer = IDPAnswer.objects.get(id=data['answerID'])
    responseData = {"score": answer.estimate_idp_score()}

    return HttpResponse(json.dumps(responseData), content_type="application/json")

