"""
    Copyright (C) 2020  KU Leuven - KRR group
    This file is part of KRR didactic tool.

    KRR didactic tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KRR didactic tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KRR didactic tool.  If not, see <https://www.gnu.org/licenses/>.
"""

import json
# SubViews:
from .submodels.notification_models import AssignmentNotification, CourseNotification
from .subviews.assignment_views import *
from .subviews.course_views import *
from .subviews.profile_and_registration_views import *


@login_required
def home(request):
    """
    Render the home screen by listing all the assignments from courses in which the logged in user is registered.
    The assignments will be listed in ascending end_date order. Only assignments whose deadline is not yet passed
    will be rendered.
    """
    assignments_left = Assignment.objects.filter(course__in=request.user.course_set.all(),end_date__gte=timezone.now()).order_by('end_date')

    context = {
        'assignments': assignments_left,
        'courses': request.user.course_set.all()
    }

    return render(request, 'students/home.html', context)


@login_required
def notifications(request):

    context = {
        'courses': request.user.course_set.all(),
        'assignment_notifications': request.user.assignmentnotification_set.all().order_by('-created_at'),
        'course_notifications': request.user.coursenotification_set.all().order_by('-created_at')
    }

    return render(request, 'students/notifications.html', context)

@login_required
def delete_notification(request, **kwargs):
    data = literal_eval(request.body.decode("utf-8"))

    responseData = {"message":"Item is deleted!", "status": 1}

    try:
        if (data['AssignmentNotificationID']) != -1:
            AssignmentNotification.objects.filter(id=data['AssignmentNotificationID']).delete()
        elif (data['CourseNotificationID']) != -1:
            CourseNotification.objects.filter(id=data['CourseNotificationID']).delete()
    except Exception as e: 
        responseData = {"message": str(e), "status": -1}

    nb = AssignmentNotification.objects.filter(user=request.user).count() + CourseNotification.objects.filter(user=request.user).count()

    responseData["notifications"] = nb
        
    return HttpResponse(json.dumps(responseData), content_type="application/json")
