"""
    Copyright (C) 2020  KU Leuven - KRR group
    This file is part of KRR didactic tool.

    KRR didactic tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KRR didactic tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KRR didactic tool.  If not, see <https://www.gnu.org/licenses/>.
"""

from students.submodels.notification_models import AssignmentNotification, CourseNotification
from django.conf import settings

def number_of_notifications(request):
    try:
        nb = AssignmentNotification.objects.filter(user=request.user).count() + CourseNotification.objects.filter(user=request.user).count()
        return {
            'nb_notifications': nb
        }
    except Exception as e:
        return {}

def issue_tracker_url(request):
    try:
        return {
            'issue_tracker_url': settings.ISSUE_TRACKER_URL
        }
    except Exception as e:
        return {}