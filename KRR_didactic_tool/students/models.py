"""
    Copyright (C) 2020  KU Leuven - KRR group
    This file is part of KRR didactic tool.

    KRR didactic tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KRR didactic tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KRR didactic tool.  If not, see <https://www.gnu.org/licenses/>.
"""

# TODO: decide what to keep here, and what to move to an eventual separate "tool" app
from PIL import Image

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse
from django.apps import apps

# SubModels:
from .submodels.answer_models import *
from .submodels.assignment_models import *
from .submodels.course_models import *
from .submodels.notification_models import *
from .submodels.question_models import *

#Override user str function
def get_full_name(self):
    return self.first_name + " " + self.last_name + "(" + self.username + ")"

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default-profile.jpg', upload_to='profile_pics')
    User.add_to_class("__str__", get_full_name)
    
    def __str__(self):
        return f'{self.user.username} Profile'

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

@register.filter(name='clearPath')
def clearPath(value):
    """
        Return list of path parts names and links and IDs if there are IDs
    """
    path_elements = list(filter(None, value.split("/")))
    new_elements = list()

    for index, elem in enumerate(path_elements):

        abs_path = "/".join(path_elements[0:index+1])
        print(abs_path)

        if elem.isdigit():
            prev = path_elements[index-1]
            class_name = prev[:-1] if prev[-1] == "s" else prev
            try:
                Class = apps.get_model(app_label='students', model_name=class_name)
                obj = Class.objects.get(pk=elem)
                new_elements.append((str(obj), abs_path))
            except:
                new_elements.append((class_name, abs_path))
        else:
            new_elements.append((elem, abs_path))

    return new_elements


@register.filter(name='idpCode')
def idpCode(value):
    """
    Retrun the string with tab added in front of each line.
    """

    to_replace = [
        ("<=>", '⇔'), 
        ('!', '∀'), 
        ('?', '∃'), 
        ('&', '∧'), 
        ('|', '∨'), 
        ('~', '¬'), 
        ("<-", '←'), 
        ("=>", '⇒'), 
        ("<=", '⇐'),  
        (">=", '≥'), 
        ("=<", '≤'), 
        ("~=", '≠')]
    
    if value:
        for (x, y) in to_replace:
            value = value.replace(x, y)

        return ('\t'.join(('\n'+value.lstrip()).splitlines(True))) + '\n'

    else:
        return ""