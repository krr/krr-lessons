"""
    Copyright (C) 2020  KU Leuven - KRR group
    This file is part of KRR didactic tool.

    KRR didactic tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KRR didactic tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KRR didactic tool.  If not, see <https://www.gnu.org/licenses/>.
"""

from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.dispatch import receiver
from .models import Profile


@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):
    """
    Create a new profile, every time a new user is created (signal=post_save)
    When a *new* User is saved, then send this signal that is received by this receiver (function) to create a profile.
    """
    if created:
        Profile.objects.create(user=instance)  # Create profile object for this user


@receiver(post_save, sender=User)
def save_profile(sender, instance, **kwargs):
    """
    Save the corresponding profile, every time a user is saved (signal=post_save)
    When a User object gets saved, then send this signal that is received by this receiver (function) to save the profile
    """
    instance.profile.save()  # Save this profile when the user is saved
