var csrftoken = ""
var submissionID = -1

var urlSaveReview = "";
var urlIDPEstimate = "";

//For IDP highlighter
var highlight = window.csHighlight;

document.addEventListener("DOMContentLoaded", function() {
    $("#responseMessage").hide();

    //CSRF token
    csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;

    //Submission id
    submissionID = document.getElementById('pickup_submission_id').value;

    //Review URL
    urlSaveReview = document.getElementById('pickup_submission_review_save_url').value;

    //Estimate IDP URL
    urlIDPEstimate = document.getElementById('pickup_idp_estimation_url').value;

    //Highlight the IDP code (not in editors)
    execute_highlight();
});

//Save the review
function saveReview(){
    //TODO: Try to refine comments and scores collection

    //Collect text comments and scores
    var textReview = [];
    $(".textQuestionAnswerID").each(function( index ) {
        answerID =  $( this ).val() 
        textReview.push({
            AnswerID: answerID,
            Score: $("#textAnswerScore-"+answerID).val(),
            Comment: $("#textAnswerComment-"+answerID).val()
        });
    });

    //Collect choice comments and scores
    var choiceReview = [];
    $(".choiceQuestionAnswerID").each(function( index ) {
        answerID =  $( this ).val() 
        choiceReview.push({
            AnswerID: answerID,
            Score: $("#choiceAnswerScore-"+answerID).val(),
            Comment: $("#choiceAnswerComment-"+answerID).val()
        });
    });

    //Collect text comments and scores
    var idpReview = [];
    $(".idpQuestionAnswerID").each(function( index ) {
        answerID =  $( this ).val() 
        idpReview.push({
            AnswerID: answerID,
            Score: $("#idpAnswerScore-"+answerID).val(),
            Comment: $("#idpAnswerComment-"+answerID).val()
        });
    });

    review = {submissionID: submissionID, textReview: textReview, choiceReview:choiceReview, idpReview: idpReview}
    
    //Send this data with ajax to the server 
    fetch(urlSaveReview, {
        method: 'POST',
        headers: {'Content-Type': 'application/json', 'X-CSRFToken': csrftoken},
        body: JSON.stringify(review),
        mode: 'same-origin'  // Do not send CSRF token to another domain.
    }).then(function (a) {
        return a.json(); // call the json method on the response to get JSON
    })
    .then(function (json) {
        $("#responseMessage").html(json.message);
        $("#responseMessage").html(json.message);
        if(json.status == 0){
            $("#responseMessage").removeClass("alert-warning");
            $("#responseMessage").addClass("alert-success");
        }else{
            $("#responseMessage").addClass("alert-warning");
            $("#responseMessage").removeClass("alert-success");
        }
        $("#responseMessage").show();
        setTimeout(function(){ $("#responseMessage").hide(); }, 5000);
    })
}

//Get the IDP answer estimation
function estimateIDP(answer_id){
    //Send this data with ajax to the server 
    fetch(urlIDPEstimate, {
        method: 'POST',
        headers: {'Content-Type': 'application/json', 'X-CSRFToken': csrftoken},
        body: JSON.stringify({answerID:answer_id}),
        mode: 'same-origin'  // Do not send CSRF token to another domain.
    }).then(function (a) {
        return a.json(); // call the json method on the response to get JSON
    })
    .then(function (json) {
        $("#idpEstimated-"+answer_id).html("Estimated ("+json.score+")")
    })
}