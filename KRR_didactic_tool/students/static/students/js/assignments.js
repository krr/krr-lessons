var assignmentIsOpen = false
var safeToLeav = true;

//Array of editors for IDP questions
//We make array in order to acces editors easier later
var editors = [];

var assignmentID = -1;
var csrftoken = "";

var urlSaveAnswers = "";
var urlCheckIDP = "";

//For IDP highlighter
var highlight = window.csHighlight;

//Once the document is ready
document.addEventListener("DOMContentLoaded", function() {
    if(document.getElementById('pickup_assignment_is_open').value == "False")
        assignmentIsOpen = false;
    else
        assignmentIsOpen = true;

    //In case user would like to leav page, notify dnager
    $(window).bind('beforeunload', function(){
        console.log(assignmentIsOpen)
        if(assignmentIsOpen & !safeToLeav)
            return 'Are you sure you want to leave?';
    });

    //Change safeToLeav after 15 sec
    setTimeout(function(){ safeToLeav = false; }, 10000);

    //Enable popover at the entire page
    $('[data-toggle="popover"]').popover();
    $("#responseMessage").hide();

    //CSRF token
    csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;

    //AssignmentID
    assignmentID = document.getElementById('pickup_assignment_id').value;

    //Save URL
    urlSaveAnswers = document.getElementById('pickup_save_answer_url').value;
    
    //Check idp URL
    urlCheckIDP = document.getElementById('pickup_check_idp_url').value;

    //Init IDP editors
    initiateEditors();

    //Highlight the IDP code (not in editors)
    execute_highlight();
});


//Function for checking IDP quesiton
function checkIDP(questionID) {
    //$('#idpBody').html('<center><div class="loader"></div></center>');
    $('#resultOfCheck').hide();
    $('#resultOfCheckLoader').show();

    //IDs of editors 
    var av = "additionalVocabulary-"+questionID;
    var avs = "additionalVocabularySymbols-"+questionID;
    var th = "theory-"+questionID;

    //Get data from editors and make JSON
    var jsonData = {
        IsSubmission: "False",
        QuestionID: questionID,
        VocabularyStudent: typeof editors[av] === 'undefined' ? "" : editors[av].getValue(),
        TheoryStudentFunctional: typeof editors[avs] === 'undefined' ? "" : editors[avs].getValue(),
        TheoryStudentStatic : typeof editors[th] === 'undefined' ? "" : editors[th].getValue(),
    };

    //Send data with ajax to server
    fetch(urlCheckIDP, {
        method: 'POST',
        headers: {'Content-Type': 'application/json', 'X-CSRFToken': csrftoken},
        body: JSON.stringify(jsonData),
        mode: 'same-origin'  // Do not send CSRF token to another domain.
    }).then(response => {
        return response.json();
    }).then(data => {

        //Display errors
        if (data["error"] == ""){
            $('#errors-result-row').hide()
        }else{
            $('#errors-result-row').show()
            $('#errors-result-filed').html(data["error"])
        }

        //Display warnings
        if (data["warning"] == ""){
            $('#warnings-result-row').hide()
        }else{
            $('#warnings-result-filed').html(data["warning"])
            $('#warnings-result-row').show()
        }

        //Unsound models
        $('#unsound-models-result-filed').html("<code class='code-wrap'>"+data["unsound"]+"</code>");
        //Change the color of card or hide if there is no result
        if (data["unsound"] == ""){
            $('#unsound-models-result-card').hide()
        }else if(data["unsound"].startsWith("Number of models: 0")){
            switch_success_danger_card("unsound-models-result", false);
        } else {
            switch_success_danger_card("unsound-models-result", true);
        }            
        
        //Incomplete models
        $('#incomplete-models-result-filed').html("<code class='code-wrap'>"+data["incomplete"]+"</code>");
        //Change the color of card or hide if there is no result
        if (data["unsound"] == ""){
            $('#incomplete-models-result-card').hide()
        }else if(data["incomplete"].startsWith("Number of models: 0")){
            switch_success_danger_card("incomplete-models-result", false);
        } else {
            switch_success_danger_card("incomplete-models-result", true);
        }
        
        //Switch loader and data
        $('#resultOfCheck').show();
        $('#resultOfCheckLoader').hide();
    }).catch(err => {
        alert("ERROR!");
    });
}

//Switch the style of idp check response card
function switch_success_danger_card(cardIdPrefix, switchToDanger){
    $('#'+cardIdPrefix+'-card').show()
    if(switchToDanger){
        $('#'+cardIdPrefix+'-card').addClass("border-danger");
        $('#'+cardIdPrefix+'-card').removeClass("border-success");

        $('#'+cardIdPrefix+'-filed').addClass("text-danger");
        $('#'+cardIdPrefix+'-filed').removeClass("text-success");
    } else {
        $('#'+cardIdPrefix+'-card').addClass("border-success");
        $('#'+cardIdPrefix+'-card').removeClass("border-danger");

        $('#'+cardIdPrefix+'-filed').addClass("text-success");
        $('#'+cardIdPrefix+'-filed').removeClass("text-danger");
    }
}

//Save answers
function saveAnswers(submit){
    //Collect text answers
    var textAnswers = [];
    $(".textQuestionCard").each(function( index ) {
        var questionID = $(this).attr("name");

        textAnswers.push({
            QuestionID: questionID,
            TextAnswer: $('#textAnswer-'+questionID).val(),
        });
    });

    //Collect choice answers
    var choiceAnswers = [];
    $(".choiceQuestionCard").each(function( index ) {
        var questionID = $(this).attr("name");

        var inputname = 'choiceAnswer-'+questionID
        var options = [];
        $.each($("input[name='"+inputname+"']:checked"), function(){
            options.push($(this).val());
        });

        choiceAnswers.push({
            QuestionID: questionID,
            ChoiceAnswer: options,
        });
    });

    //Collect idp answers 
    var idpAnswers = [];
    
    $(".idpQuestionCard").each(function( index ) {
        var questionID = $(this).attr("name");

        var av = "additionalVocabulary-"+questionID;
        var avs = "additionalVocabularySymbols-"+questionID;
        var vocabularyStudent = typeof editors[av] === 'undefined' ? "" : editors[av].getValue();
        var theoryStudentFunctional = typeof editors[avs] === 'undefined' ? "" : editors[avs].getValue();
        var theoryStudentStatic = editors["theory-"+questionID].getValue();

        idpAnswers.push({
            QuestionID: questionID,
            VocabularyStudent: vocabularyStudent,
            TheoryStudentFunctional: theoryStudentFunctional,
            TheoryStudentStatic : theoryStudentStatic
        });
    });

    //Send this data with ajax to the server 
    answers = {assignmentID: assignmentID, textAnswers: textAnswers, choiceAnswers:choiceAnswers, idpAnswers: idpAnswers, submit: submit}
    fetch(urlSaveAnswers, {
        method: 'POST',
        headers: {'Content-Type': 'application/json', 'X-CSRFToken': csrftoken},
        body: JSON.stringify(answers),
        mode: 'same-origin'  // Do not send CSRF token to another domain.
    }).then(function (a) {
        return a.json(); // call the json method on the response to get JSON
    })
    .then(function (json) {
        $("#responseMessage").html(json.message);
        $("#responseMessage").html(json.message);
        if(json.status == 0){
            $("#responseMessage").removeClass("alert-warning");
            $("#responseMessage").addClass("alert-success");
            safeToLeav = true;
        }else{
            $("#responseMessage").addClass("alert-warning");
            $("#responseMessage").removeClass("alert-success");
        }
        $("#responseMessage").show();
        setTimeout(function(){ $("#responseMessage").hide(); }, 10000);
    })
}

//Create an asociative array where key is textarea ID and value is reference to codemirror editor 
function initiateEditors(){
    Array.from(
        //Selector for textareas that should be codemirror editors
        document.querySelectorAll('[id^="additionalVocabulary-"],[id^="additionalVocabularySymbols-"],[id^="theory-"]'))
        .forEach(function (x) { 
            //Init editor per textarea
            var e = CodeMirror.fromTextArea(x,{
                mode: "idp",
                lineNumbers: true,
                lineWrapping: true,
                theme: "duotone-light",
            });  

            //Add editor to array
            editors[x.id] = e;
        }
    ); 
}

//This function serves to refresh editors when they are unhidden
function openIDPQuestions() {
    //There must be small timeout because of animation
    setTimeout(function() {
        for (var key in editors) {
            editors[key].refresh();
        }
    },1);

    closePopovers();
}

//Insert the text to editor
function insertTextAtCursor(selector, questionID, text) {

    //Get editor
    var editorID = selector + "-" + questionID;
    editor = editors[editorID];

    //Replace the text
    var doc = editor.getDoc();
    var cursor = doc.getCursor();
    doc.replaceRange(text, cursor);

    //Bring the cursor back
    editor.focus();
    editor.setCursor(doc.getCursor());
}

//Function that closes all popovers on the page
function closePopovers() {
    $('[data-toggle="popover"]').popover('hide');
}