"""
    Copyright (C) 2020  KU Leuven - KRR group
    This file is part of KRR didactic tool.

    KRR didactic tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KRR didactic tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KRR didactic tool.  If not, see <https://www.gnu.org/licenses/>.
"""

from django.contrib import admin
from django.contrib.auth.models import User

from .models import Profile, Course, Assignment, TextQuestion, ChoiceQuestion, IDPQuestion, Submission, Notification, \
    AssignmentNotification, CourseNotification
from .submodels.answer_models import Answer, TextAnswer, ChoiceAnswer, IDPAnswer
from .submodels.question_models import Option
import nested_admin


admin.site.site_header = 'KRR administration'


class TextQuestionInline(nested_admin.NestedStackedInline):
    model = TextQuestion
    # exclude = ('student_answer',)
    extra = 0


class OptionInline(nested_admin.NestedTabularInline):
    model = Option
    extra = 0


class ChoiceQuestionInline(nested_admin.NestedStackedInline):
    model = ChoiceQuestion
    extra = 0
    inlines = [OptionInline]


class IDPQuestionInline(nested_admin.NestedStackedInline):
    model = IDPQuestion
    extra = 0


class AssignmentAdmin(nested_admin.NestedModelAdmin):
    list_display = ("title", "course", "author", "start_date", "end_date", "created_at", "updated_at")
    list_filter = ("start_date", "end_date", "created_at", "updated_at")
    search_fields = ("title", "author")
    fieldsets = [
        (None,          {'fields': ['title', 'description', 'course']}), # 'author' removed from fields
        ('Date information', {'fields': ['start_date', 'end_date']}),  # 'classes': ['collapse']
    ]
    readonly_fields = ['author']
    inlines = [TextQuestionInline, ChoiceQuestionInline, IDPQuestionInline]
    save_as = True

    def save_model(self, request, obj, form, change):
        """This method is needed to set the author to the current logged in user before saving the model object"""
        #Check if object is created or updated
        update = False
        if obj.id:
            update = True

        #Add author to be the user that created 
        if not update:
            obj.author = request.user
        
        #Save the object
        obj.save()

        #Notify the students
        obj.notify_students(update)

class CourseAdmin(nested_admin.NestedModelAdmin):
    list_display = ("name", "start_date", "created_at", "updated_at")
    list_filter = ("start_date","created_at", "updated_at")
    search_fields = ("name","created_at", "updated_at")

    def save_model(self, request, obj, form, change):
        """This method is needed to create a CourseNotification on the creation of a Course"""
        #Check if object is created or updated
        update = False
        if obj.id:
            update = True

        #Save object
        obj.save()

        #Notify the students
        obj.notify_students(update)


admin.site.register(Assignment, AssignmentAdmin)
admin.site.register(Course, CourseAdmin)
admin.site.register(AssignmentNotification)
admin.site.register(CourseNotification)

