"""
    Copyright (C) 2020  KU Leuven - KRR group
    This file is part of KRR didactic tool.

    KRR didactic tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KRR didactic tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KRR didactic tool.  If not, see <https://www.gnu.org/licenses/>.
"""


"""
KRR_didactic_tool URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from students import views as students_views
urlpatterns = [
    path('', students_views.home, name='home'),
    path('admin/', admin.site.urls),
    # Notifications
    path('notifications/', students_views.notifications, name='notifications'),
    path('notifications/delete', students_views.delete_notification, name='notifications-delete'),
    # Registrations Login/Logout and Profile
    path('register/', students_views.register, name='register'),
    path('profile/', students_views.profile, name='profile'),
    path('login/', auth_views.LoginView.as_view(template_name='students/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='students/logout.html'), name='logout'),
    #Password reset
    path('password_reset/', auth_views.PasswordResetView.as_view(), name='password_reset'),
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
    # Courses
    path('courses/', students_views.CourseListView.as_view(), name='courses'),
    path('courses/<int:pk>', students_views.CourseDetailView.as_view(), name='course-detail'),
    path('courses/<int:pk>/register', students_views.CourseRegisterView.as_view(), name='course-register'),
    path('courses/<int:pk>/unregister', students_views.CourseUnregisterView.as_view(), name='course-unregister'),
    # Assignments
    path('assignments/', students_views.AssignmentListView.as_view(), name='assignments'),
    path('assignments/<int:pk>', students_views.AssignmentDetailView.as_view(), name='assignment-detail'),
    path('assignments/<int:pk>/submissions', students_views.AssignmentSubmissionsListView.as_view(), name='assignment-submissions'),
    path('assignments/<int:assignment_id>/submissions/<int:submission_id>', students_views.AssignmentSubmissionView.as_view(), name='assignment-submission-detail'),
    #Ajax
    path('assignments/saveAnswers', students_views.save_answers, name='assignment-answers-save'),
    path('assignments/checkIDP', students_views.check_idp, name='check-idp'),
    path('assignments/saveSubmitionReview', students_views.save_submission_review, name='submission-review-save'),
    path('assignments/automatedChecking', students_views.automated_checking, name='automated-checking'),
    path('assignments/idpEstimation', students_views.idp_estimation, name='idp-estimation'),
    # Submissions
    path('submissions/', students_views.SubmissionsListView.as_view(), name='submissions')
]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)