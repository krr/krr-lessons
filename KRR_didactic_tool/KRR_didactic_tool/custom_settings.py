"""
    Copyright (C) 2020  KU Leuven - KRR group
    This file is part of KRR didactic tool.

    KRR didactic tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KRR didactic tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KRR didactic tool.  If not, see <https://www.gnu.org/licenses/>.
"""


import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

#Allowed host namse
ALLOWED_HOSTS = []


# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# My sql database settings
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'NAME': 'dbname',
#         'USER': 'user',
#         'PASSWORD': 'password',
#         'HOST': '',
#         'PORT': '',
#         'OPTIONS': { 'charset': 'utf8' }
#     }
# }


# Static files
STATIC_ROOT = os.path.join(BASE_DIR, 'static') #Here the static files will be exported 
STATIC_URL = '/static/'

#Media files
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')  # Where to store uploaded files (in file-system, not database)
MEDIA_URL = '/media/'

# Email SMTP Integration
EMAIL_HOST = 'smtp.mailtrap.io'
EMAIL_HOST_USER = 'cef7fb451668df'
EMAIL_HOST_PASSWORD = '9150cf42395e20'
EMAIL_PORT = '2525'

#KBS Web service endpoint
KBSWS_ENDPOINT = 'http://localhost:3333/process'
KBSWS_NUMBER_OF_MODELS = 1
ERROR_KEY_WORDS = ['Could not derive', 'Term', 'syntax error', 'Predicate', 'Function', 'Internal error', 'Static theory']


EMAIL_NOTIFICATIONS = False

ISSUE_TRACKER_URL = 'https://bitbucket.org/krr/krr-lessons/issues/new'